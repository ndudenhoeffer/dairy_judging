from __future__ import unicode_literals
from collections import namedtuple
from .constants import placings

from django.db import models


class Team(models.Model):
    COUNTRIES = namedtuple('countries', ['US', 'Canada'])(
        US='US',
        Canada='Canada'
    )
    COMPETITIONS = namedtuple('competitions', ['H', 'C'])(
        H='4H',
        C='Collegiate'
    )

    name = models.CharField(max_length=128)
    number = models.PositiveIntegerField()
    competition = models.CharField(max_length=16, choices=((c,c) for c in COMPETITIONS), default=COMPETITIONS.C)
    full_name = models.CharField(max_length=128, null=True, blank=True)
    coach = models.CharField(max_length=128)
    coach_full = models.CharField(max_length=128, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    assistant = models.CharField(max_length=128, null=True, blank=True)
    country = models.CharField(max_length=16, choices=[(c,c) for c in COUNTRIES], default=COUNTRIES.US)

    def __unicode__(self):
        return self.name


class Contestant(models.Model):
    class Meta:
        unique_together = (("number", "team"),)

    name = models.CharField(max_length=128)
    team = models.ForeignKey('Team')
    number = models.SmallIntegerField()
    is_alternate = models.BooleanField()

    def get_placing_score(self):
        score = 0
        for placing in self.place_set.all():
            score += placing.score
        return score

    def get_reason_score(self):
        try:
            return self.reason_set.annotate(reason_score=models.Sum('score')).values_list('reason_score', flat=True)[0]
        except IndexError:
            return  0

    @property
    def total_score(self):
        return self.get_placing_score() + self.get_reason_score()

    @property
    def placing_count(self):
        return self.place_set.count()

    @property
    def reason_count(self):
        return self.reason_set.count()

    @property
    def team_number(self):
        return self.team.number

    @property
    def competition(self):
        return self.team.competition

    @property
    def team_name(self):
        return self.team.name

    def __unicode__(self):
        return '{0}-{1}  {2}'.format(self.team.number, self.number, self.name)


class ShowClass(models.Model):
    AYRSHIRE = 'AY'
    BROWNSWISS = 'BS'
    GUERNSEY = 'GU'
    HOLSTEIN = 'HO'
    JERSEY = 'JE'
    MILKINGSHORTHORN = 'MS'
    REDWHITE = 'RW'

    BREEDS = (
        (AYRSHIRE, 'Ayrshire'),
        (BROWNSWISS, 'Brown Swiss'),
        (GUERNSEY, 'Guernsey'),
        (HOLSTEIN, 'Holstein'),
        (JERSEY, 'Jersey'),
        (MILKINGSHORTHORN, 'Milking Shorthorn'),
        (REDWHITE, 'Red & White'),
    )

    COW = 'cow'
    HEIFER = 'heifer'
    REASON = 'reason'
    CLASS_TYPES = (
        (COW, COW),
        (HEIFER, HEIFER),
        (REASON, REASON)
    )

    breed = models.CharField(max_length=32, choices=BREEDS)
    class_type = models.CharField(max_length=16, choices=CLASS_TYPES)
    description = models.TextField()
    place_1 = models.PositiveSmallIntegerField()
    place_2 = models.PositiveSmallIntegerField()
    place_3 = models.PositiveSmallIntegerField()
    place_4 = models.PositiveSmallIntegerField()
    cut_1 = models.PositiveSmallIntegerField()
    cut_2 = models.PositiveSmallIntegerField()
    cut_3 = models.PositiveSmallIntegerField()

    def get_position(self, cow):
        for position in (1, 2, 3, 4):
            if getattr(self, 'place_{0}'.format(position)) == int(cow):
                return position
        raise ValueError('Cow not found in class: {0}'.format(cow))

    def get_cut(self, top, bottom):
        top_position = self.get_position(top)
        bottom_position = self.get_position(bottom)
        deduct = 0
        for cut in (1, 2, 3):
            if top_position > cut and bottom_position <= cut:
                deduct += getattr(self, 'cut_{0}'.format(cut))
        return deduct

    def __unicode__(self):
        return '{0} - {1}'.format(self.breed, self.class_type)


class Place(models.Model):
    contestant = models.ForeignKey('Contestant')
    show_class = models.ForeignKey('ShowClass', limit_choices_to=models.Q(class_type__in=(ShowClass.COW, ShowClass.HEIFER)))
    place = models.PositiveSmallIntegerField(choices=((key, value) for key, value in placings.iteritems()))

    @property
    def placing(self):
        return placings[self.place]

    @property
    def score(self):
        placing_string = self.placing
        place_1 = placing_string[0]
        place_2 = placing_string[1]
        place_3 = placing_string[2]
        place_4 = placing_string[3]

        score = 50
        for top, bottom in (
            (place_1, place_2),
            (place_1, place_3),
            (place_1, place_4),
            (place_2, place_3),
            (place_2, place_4),
            (place_3, place_4),
        ):
            score -= self.show_class.get_cut(top, bottom)
        return score

    def __unicode__(self):
        return '{0} - {1}'.format(self.placing, self.contestant)


class Reason(models.Model):
    contestant = models.ForeignKey('Contestant')
    show_class = models.ForeignKey('ShowClass', limit_choices_to={'class_type': ShowClass.REASON})
    score = models.PositiveSmallIntegerField()
