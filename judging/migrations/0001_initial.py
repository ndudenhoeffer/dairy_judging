# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-03 21:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contestant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('number', models.SmallIntegerField()),
                ('is_alternate', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('place', models.PositiveSmallIntegerField(choices=[(1, b'1'), (2, b'2'), (3, b'3'), (4, b'4'), (5, b'5'), (6, b'6'), (7, b'7'), (8, b'8'), (9, b'9'), (10, b'10'), (11, b'11'), (12, b'12'), (13, b'13'), (14, b'14'), (15, b'15'), (16, b'16'), (17, b'17'), (18, b'18'), (19, b'19'), (20, b'20'), (21, b'21'), (22, b'22'), (23, b'23'), (24, b'24')])),
                ('contestant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='judging.Contestant')),
            ],
        ),
        migrations.CreateModel(
            name='Reason',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('score', models.PositiveSmallIntegerField()),
                ('contestant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='judging.Contestant')),
            ],
        ),
        migrations.CreateModel(
            name='ShowClass',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('breed', models.CharField(choices=[('AY', 'Ayrshire'), ('BS', 'Brown Swiss'), ('GU', 'Guernsey'), ('HO', 'Holstein'), ('JE', 'Jersey'), ('MS', 'Milking Shorthorn'), ('RW', 'Red & White')], max_length=32)),
                ('class_type', models.CharField(choices=[('cow', 'cow'), ('heifer', 'heifer'), ('reason', 'reason')], max_length=16)),
                ('place_1', models.PositiveSmallIntegerField()),
                ('place_2', models.PositiveSmallIntegerField()),
                ('place_3', models.PositiveSmallIntegerField()),
                ('place_4', models.PositiveSmallIntegerField()),
                ('cut_1', models.PositiveSmallIntegerField()),
                ('cut_2', models.PositiveSmallIntegerField()),
                ('cur_3', models.PositiveSmallIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('full_name', models.CharField(blank=True, max_length=128, null=True)),
                ('coach', models.CharField(max_length=128)),
                ('coach_full', models.CharField(blank=True, max_length=128, null=True)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('assistant', models.CharField(blank=True, max_length=128, null=True)),
                ('country', models.CharField(choices=[('US', 'US'), ('Canada', 'Canada')], default='US', max_length=16)),
            ],
        ),
        migrations.AddField(
            model_name='reason',
            name='show_class',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='judging.ShowClass'),
        ),
        migrations.AddField(
            model_name='place',
            name='show_class',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='judging.ShowClass'),
        ),
        migrations.AddField(
            model_name='contestant',
            name='team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='judging.Team'),
        ),
    ]
