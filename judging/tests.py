from django.test import TestCase
from .models import ShowClass

# Create your tests here.
class CutCalcCase(TestCase):

    def test_get_deduct(self):
        show_class = ShowClass(
            breed=ShowClass.HOLSTEIN,
            class_type=ShowClass.COW,
            place_1=2,
            place_2=1,
            place_3=4,
            place_4=3,
            cut_1=1,
            cut_2=6,
            cut_3=4
        )

        deduct = show_class.get_cut(3, 4)
        self.assertEqual(deduct, 4)
