from django.contrib import admin
from .models import Team, Contestant, ShowClass, Place, Reason
# Register your models here.


class ContestAdmin(admin.ModelAdmin):
    list_display = (
        'number',
        'team_number',
        'competition',
        'team_name',
        'placing_count',
        'reason_count',
        'total_score',
    )

admin.site.register(Team)
admin.site.register(Contestant, ContestAdmin)
admin.site.register(ShowClass)
admin.site.register(Place)
admin.site.register(Reason)
