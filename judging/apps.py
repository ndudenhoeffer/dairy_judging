from __future__ import unicode_literals

from django.apps import AppConfig


class JudgingConfig(AppConfig):
    name = 'judging'
